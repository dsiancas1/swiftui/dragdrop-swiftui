//
//  DropViewDelegate.swift
//  SampleSwiftUI
//
//  Created by Daniel Siancas on 18/01/21.
//

import SwiftUI

struct DropViewDelegate: DropDelegate {
    
    var page: Page
    var pageData: PageViewModel
    
    func performDrop(info: DropInfo) -> Bool {
        return true
    }
    
    func dropEntered(info: DropInfo) {
        let fromIndex = pageData.urls.firstIndex(where: { $0.id == pageData.currentPage?.id }) ?? 0
        
        let toIndex = pageData.urls.firstIndex(where: { $0.id == self.page.id }) ?? 0
        
        if fromIndex != toIndex {
            withAnimation(.default) {
                let fromPage = pageData.urls[fromIndex]
                pageData.urls[fromIndex] = pageData.urls[toIndex]
                pageData.urls[toIndex] = fromPage
            }
        }
    }
    
    func dropUpdated(info: DropInfo) -> DropProposal? {
        return DropProposal(operation: .move)
    }
}
