//
//  Page.swift
//  SampleSwiftUI
//
//  Created by Daniel Siancas on 18/01/21.
//

import Foundation

struct Page: Identifiable {
    var id = UUID().uuidString
    var url: URL
}
