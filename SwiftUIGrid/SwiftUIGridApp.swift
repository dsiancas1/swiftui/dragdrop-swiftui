//
//  SwiftUIGridApp.swift
//  SwiftUIGrid
//
//  Created by Daniel Siancas on 18/01/21.
//

import SwiftUI

@main
struct SwiftUIGridApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
